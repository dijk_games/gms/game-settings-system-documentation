# Game Settings System

Save your game configuration without pain and whitout wasting time

Dont save your game settings whit your player game saves, this is the solution you want to accelerate your development

![example](img/05.png)

## Content
- [Getting starter](getting_started.md)
- [Game Settings System object reference](GameSettingsSystem.md)
- [Settings object reference](Settings.md)
