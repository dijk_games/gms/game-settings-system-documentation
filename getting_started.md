# Getting started

## Configuration

### File configuration

The first thing you must to do is to configure your script

Open the script gss_init to:
- Configure the file name
- Configure the file type

![settings](img/01.png)

Change the predefined file name in the global variable `global.gss_gameSettingsFile` (you dont need to include the file extension)

Change the file type in the global variable `global.gss_fileFormat`. Here you can choose the following values
- global.gss_FILE_TYPE_INI (Creates an INI file, human readable)
- global.gss_FILE_TYPE_DAT (Creates a DAT binary file)

### Defaults
When your game starts must verify if it's the first run to create the initial configuration file, you can do it in the script `gss_createDefaultConfig`

Here we have a configuration example to explan you how you can save a configuration, feel free to modify it as you want.

![defaults](img/03.png)

Now, let's review the code

The first thing you needs to do is to create a settings object to store your default settings `settings = new gss_Settings("SETTINGS");`

After creating the settigns object you must create settigns sections to store your settings, there is an example:

```GML
// General settings
	general = settings.create("GENERAL");	// Create the GENERAL section
	general.set("dificulty",0);				// 0 - Easy, 1 - Normal, 2 - Hard, 3 - Insane
	general.set("lang","en");				// Game language
	general.set("sub_lang","en");			// Subtitles language
	general.set("show_sub",0);				// Show subtitles? 0 - false, 1 true
```

All of this will create something like this:

```JSON
{
  "VIDEO": {
    "show_fps": 0.000000,
    "window": 0.000000,
    "width": "800",
    "height": "600"
    },
  "GENERAL": {
    "show_sub": 0.000000,
    "dificulty": 0.000000,
    "sub_lang": "en",
    "lang": "en"
    },
  "SOUND": {
    "dial_volume": 100.000000,
    "fx_volume": 100.000000,
    "music_volume": 100.000000
  }
}
```
And if the file type is set to INI File will store a file like this `settings.ini`

```INI
[VIDEO]
show_fps="0.000000"
window="0.000000"
width="1024"
height="600"
[GENERAL]
show_sub="0.000000"
dificulty="0.000000"
sub_lang="en"
lang="en"
[SOUND]
dial_volume="100.000000"
fx_volume="100.000000"
music_volume="100.000000"

```

### Saving and Loading settings

You must create a object or a script to manage your settings, in this example we use an empty object

![step1](img/02.png)

Now create a create event

![step2](img/04.png)

Store an instance of Game Settings System to modify, load or save your settings

![step3](img/05.png)

Let's review the code

The first step is to get an instance of Game Settings System

```GML
gs = gss_getInstance();					// Game settings Instance
```

Get the settings object

```GML
settings = gs.getSettings();				// Get the settings
```

Get the section you want to modify

```GML
video = settings.get("VIDEO");				// Get de video settings
```


Modify and update it

```GML
video.set("width","1024");					// change the width
settings.update("VIDEO",video);				// Update your settings section
```

Save in a file

```GML
gs.save();
```

Learn more about:
 - [Game Settings System object](GameSettingsSystem.md)
 - [Settings object](Settings.md)
